# Repositories mit HTTPS Clonen

Sie können Repositories über das HTTPS Protokoll synchronisieren. Der Erste Schritt ist üblichserweise das Clonen, also das Kopieren eines bestehenden Repositories von einem Server.

Öffenen Sie die Startseite Ihres Projekts in GitLab. Im rechten Bereich der Seite finden Sie den blau hervorgehobenen Button `Clone`. Dort können Sie die URL `Clone with HTTPS` kopieren.

![](img/gitlab_clone_repository.png)

Öffnen Sie die Kommandozeile (GitBash unter Windows, ein beliebiges Terminal unter Mac OS oder Linux), um mit git auf ihrem Computer zu arbeiten.

Erstellen Sie einen neuen, leeren Ordner. Zum Beispiel mit dem Kommando `mkdir workshop`.

Wechseln Sie in den Ordner mit dem Kommando `cd workshop`.

Laden Sie das Projekt mit dem Kommando `git clone [URL]` herunter.

Zum Herunterladen von Daten aus dem Repositoy benötigt `git` Ihren Nutzernamen und Passwort.

Sie können i.d.R. Ihren GitLab Nutzernamen und Passwort verweden. Allerdings wird stattdessen die Verwendung von sog. *Personal Access Tokens* empfohlen.

Um ein *Personal Access Token* anzulegen öffnen Sie Ihre Profileinstellungen in GitLab. Dort finden Sie den Punkt [Access Tokens](https://gitlab.com/-/profile/personal_access_tokens).

Sie können dem Token dort einen Namen geben. Nutzen Sie z.B. den Computernamen um das Token später eindeutig zuordnen zu können.
Selektieren Sie die *Scopes* `read_repository` und `write_repository` und klicken Sie dann auf den Button zum Erstellen des *Personal Access Tokens*.

![](img/gitlab_create_pat.png)

Im Anschluss zeigt GitLab Ihnen das *Personal Access Token* auf der Webseite an.
Kopieren Sie das Token (z.B. in Ihren Passwort-Manager), da es beim neu laden der Seite nicht erneut angezeigt wird.
Sie können die *Personal Access Tokens* jederzeit löschen oder neue erstellen, wenn Sie das Token verloren oder vergessen haben.

Nutzen Sie das *Personal Access Token* jetzt als Passwort für den `git clone` Befehl.
Als Nutzername verwenden Sie Ihren GitLab Nutzernamen, den Sie ebenfalls in den Profileinstellungen unter dem Menüpunkt [Account](https://gitlab.com/-/profile/account) einsehen oder ändern können.
