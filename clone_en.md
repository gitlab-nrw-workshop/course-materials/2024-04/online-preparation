# Cloning Repositories with HTTPS

You can keep your repositories synchronized using the HTTPS protocol.
Usually, the first step is cloning, i.e. getting a copy of the repository from a remote server.

Navigate to the main page of your GitLab project (`Project Overview`).
On the right side of the page you will find a blue button labeled `Clone`.
There you can copy the URL shown under `Clone with HTTPS`.

![](img/gitlab_clone_repository.png)

Open the command line (GitBash on Windows, any terminal on Mac OS or Linux) to work with Git on your device.

Create a new empty folder, for example using the command `mkdir workshop`.

Navigate into that folder using the command `cd workshop`.

Download the repository using the command `git clone [URL]`.

Usually, `git` needs your GitLab user name and password for this.
In certain cases, e.g. when you have enabled two-factor authentication (2FA) for your account, you will need to create a *Personal Access Tokens*.

To create a *Personal Access Tokens*, open your GitLab profile.
There, you will find the section [Access Tokens](https://gitlab.com/-/profile/personal_access_tokens).
 
You can assign a name to the token, e.g. the name of your computer, so that you can tell apart multiple tokens.
Select the *Scopes*  `read_repository` and `write_repository`.
Then click on `Create Personal Access Token` to generate the token.

![](img/gitlab_create_pat.png)

GitLab will show you the token on the top of the website.
Copy the token to a safe place, e.g. a password manager, since it is only shown once.
In case you loose or forget this token, you can delete existing *Personal Access Tokens* or create new ones at any time.

When cloning the repository using `git clone`, the *Personal Access Token* acts as your password.
The user name is your GitLab user name, which you can also find on the profile page under the section [Account](https://gitlab.com/-/profile/account).
