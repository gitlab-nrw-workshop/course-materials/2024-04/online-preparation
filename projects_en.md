# Creating and Configuring GitLab Projects

GitLab allows to create projects to manage your files and code.
In addition to the git repository, which is the core of a project, each project offers features like issue management, wikis, or continuous integration.
A project can be public (visible without login), internal (visible for user that is logged in), or private (only visible to members of the project).
Further, you can create groups for specific tasks or user groups and create projects for these groups.

## Creating Projects
1. On the main page, click the `New Project` button on the right side of the page or use the plus-icon from the navigation bar.
2. Specify if you want to create an empty project or use a project template.
   Project template can make some configurations and already contain files for the specific use case.
   Finally, you can also import an existing project (e.g. from GitLab, GitHub, etc.).
3. Pick `Empty Project` and enter the following information:
   * Enter a project name. Note, that some characters are not allowed in project names, spaces, dashes, underlines, and emoji, however, can be used.
   * The field `Project Description (optional)` allows you to enter a description that is shown on the dash board of your project.
     This can be very helpful to communicate the purpose or content of your project.
   * Pick a visibility level as described above. This governs who can see and interact with your project.
   * Decide if you want to initialize your project with a `README` file.
     This is a good way to start, since it automatically creates a git repository with a main branch and a first commit.
     Otherwise, you will be shown a guide on how to initialize a repository.
4. Click on `Create Project`.

## Project Settings

Only users that hold the roles `Maintainer` or `Owner` in a repository can change the project settings.

You can do by clicking on `Settings` on the left side of the project home page.
There, you will see a list features for which you can supply settings.
Most important for now are the `General` and `Repository` settings.
You can safely skip the rest for now.

### General

* General project settings: These adapt meta information like name, description, and avatar image of your project.
* Visibility, project features, permissions: Here you can change the actions specific user groups can take, e.g. who can create new issues.
  Additionally, you can switch off features that you are not using.
* Badges and Service Desk: These are advanced features for displaying information about your project and for user interaction that you can safely skip for now.
* Advanced: Here you find options to rename, transfer, export, or delete your project. Please note that renaming or transferring can have unexpected effects since these actions change the URL under which your project can be found.


### Repository

This point allows you to configure how the underlying git repository of your project behaves.
Most importantly you can specify rules for branches—different independent chains of commits within the repository.

* Mirroring repositories: Replicate the changes in this repository in another repository.
* Protected Branches and Protected Tags: This option protects certain branches from accidental changes.
* Deploy Keys and Deploy Token: These can be used for automatic interaction with the repository e.g. via bots or scripts.

**Caution**: By default, the main branch of a repository is a **Protected Branch**.
This can make collaboration cumbersome and should be changed (at least when collaborating with people you trust).
Click on the `unprotect` next to the main branch to do this.


## User Management

You can manage who has access to a project by clicking on `Project Information → Members`.
Here, you can add users to your project (if they logged into GitLab before).

You can grant the following roles to users:

* Guest: Can read and write issues and read the wiki. Guests cannot see or change the files in the git repository.
* Reporter: As Guest, but can see the files in the repository.
* Developer: As reporter, but can edit files in the repository and the wiki.
* Maintainer: As Developer, but can make changes to project settings.

For a more detailed explanation of these roles, please refer to the GitLab documentation.
