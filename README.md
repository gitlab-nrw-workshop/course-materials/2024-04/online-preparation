# Introduction to RDM with git and GitLab

Datum: 2024-04-16

Format: Flipped Classroom in and with GitLab

Preparation Time: ~2h

Welcome to the online preparation for the workshop "Introduction to RDM with git and GitLab".
The goal of this preparation is to reduce the time allocated to installation and setup during the workshop.


## What are git and GitLab

[Git](https://git-scm.com/) is an established [version control system](https://en.wikipedia.org/wiki/Version_control) used primarily in software development.
The core idea of git is to track changes in text files and create a chain of versions of these files that can be restored later.
While initially a tool for software development, git can be used to version all text files, including data sets etc. stored in CSV, JSON, or XML files.

[GitLab](https://en.wikipedia.org/wiki/GitLab) is a web application used to host, manage, and organize git repositories.
In addition to a powerful online editor, GitLab offers tools for project management and automation that can be leveraged for RDM.
GitLab is offered as a service by many universities using local installations.
For easier collaboration, this workshop will take place on the publicly available cloud-based version [GitLab.com](https://gitlab.com).


## Online Preparation

As preparation for the workshop, this repository aims to ensure that we can focus on the use of git and GitLab.
It will instruct you to finish the following tasks:

- Log into the GitLab installation used for this workshop,
- install git on you computer,
- create a project,
- and execute basic git commands.

If you have any questions about any of the tasks, please create an [Issue / Ticket](https://gitlab.com/gitlab-nrw-workshop/course-materials/2024-04/online-preparation/-/issues) in this GitLab project.
We will try to reply as soon as possible.

To start with the online preparation, you can log in to [https://gitlab.com](https://gitlab.com).
There you will be able to create a checklist for your preparation work by clicking here and selecting "Submit Issue" at the bottom of the page:

:arrow_right: [Create Preparation Checklist](https://gitlab.com/gitlab-nrw-workshop/course-materials/2024-04/online-preparation/-/issues/new?issuable_template=Preparation&issue[title]=Online+Preparation) :arrow_left:

To be well-prepared for the workshop, please work through the steps marked **REQUIRED** in the checklist.
Steps marked *Optional* and *Suggested* are not required, but will make following the more complex tasks easier.
