# Installation von Git

* Offizielle Dokumentation, alle Betriebssysteme [German](https://git-scm.com/book/de/v2/Erste-Schritte-Git-installieren)
* [Installation, Windows (1:53)](https://www.youtube.com/watch?v=5KFn0r2XrtA)
* [Installationsanleitung, Windows](install_windows.md)
* [Installation, Mac OSX (2:49)](https://www.youtube.com/watch?v=TvrZw47e7tI)
* [Installation, Linux (2:13)](https://www.youtube.com/watch?v=HSOuBmiCdrM)
* [Konfigurieren (2:46)](https://www.youtube.com/watch?v=PegV5zz5iFU)
