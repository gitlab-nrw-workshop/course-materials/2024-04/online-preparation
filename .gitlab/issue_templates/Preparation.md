<!--

You are creating a personal checklist to track your progress for the online preparation.

Click on the button "Create Issue" to create your checklist.

On the following screen, you will see your checklist.































-->

This online preparation comprises videos, tutorials, and small step-by-step tasks.

Once you completed a task you can mark it **DONE** by clicking the associated checkbox.

You will now find you personal checklist under the current URL or by using this [link](../../issues?search=Online+Preparation).

#### Basics

* [x] Log into [GitLab.com](https://gitlab.com)
* [x] [Create Checklist](../../issues?search=Online+Preparation)

#### **[REQUIRED]** Installing and configuring Git

* [ ] Install Git on your device
  * [Video Guide, Mac OSX (2:49), German](https://www.youtube.com/watch?v=TvrZw47e7tI)
  * [Video Guide, Windows (1:53), German](https://www.youtube.com/watch?v=5KFn0r2XrtA)
  * [Video Guide, Linux (2:13), German](https://www.youtube.com/watch?v=HSOuBmiCdrM)
  * [Written Guide, Windows](install_windows.md)
  * Official Documentation, all OS [German](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git), [English](https://git-scm.com/book/de/v2/Erste-Schritte-Git-installieren)
* [ ] Configure your `user.name` and `user.email` for Git
  * [Video Guide (2:46), German](https://www.youtube.com/watch?v=PegV5zz5iFU)
  * Official Documentation [German](https://git-scm.com/book/de/v2/Erste-Schritte-Git-Basis-Konfiguration) ("Ihre Identität"), [English](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) ("Your Identity")

#### *[Optional]* Graphical User Interfaces for Git
These GUIs for Git are not required for the workshop but can make working with Git a bit more convenient.

* [ ] [Installation Guide, TortoiseGit, English](install_tortoise.md)
* [ ] [Installation Guide, VSCodium, English](install_vscode.md)

#### *[SUGGESTED]* Basics of version control with Git
These video tutorial show how to work with Git on the command line.
While the German version is split up into single videos, the english video is one long continuous video.

* German
  * [ ] [Lokales Repository Anlegen, Mac OSX/ Linux (2:51), German](https://www.youtube.com/watch?v=Bo-pKqHO2go)
  * [ ] [Lokales Repository Anlegen, Windows (6:34), German](https://www.youtube.com/watch?v=8Qau5_NmF9s)
  * [ ] [Änderungen Machen (3:13), German](https://www.youtube.com/watch?v=0ya5jueUlqs)
  * [ ] [Änderungen Überprüfen (1:15), German](https://www.youtube.com/watch?v=XyrnJNrI3cQ)
  * [ ] [Änderungen Committen (3:02), German](https://www.youtube.com/watch?v=mg82bvto4Ug)
  * [ ] [Versionen Vergleichen (2:50), German](https://www.youtube.com/watch?v=-9hyURYmvsY)
* English
  * [ ] [Creating local repositories (2:32)](https://youtu.be/8JJ101D3knE?t=879)
  * [ ] [Changing files and tracking changes (3:48)](https://www.youtube.com/watch?v=8JJ101D3knE&t=1306s)
  * [ ] [Commiting changes (2:13)](https://www.youtube.com/watch?v=8JJ101D3knE&t=1524s)
  * [ ] [Comparing versions (5:00)](https://www.youtube.com/watch?v=8JJ101D3knE&t=2733s)


#### *[SUGGESTED]* Collaboration with GitLab
Translations of these pages will follow shortly. Sorry for the inconvenience.

* [ ] Creating and configuring GitLab projects [German](projects.md), [English](projects_en.md)
* [ ] Documentation, Markdown und Wikis [German](documentation.md)
* [ ] Planning and tracking work packages and projects [German](issues.md)


#### Working with GitLab projects

After finished the **REQUIRED** and **SUGGESTED** preparation steps above, please try to work through the following steps.
Try to get as far as possible, but don't worry, all these steps will be repeated in the workshop.

* Log into [GitLab.com](https://gitlab.com)
    * [ ] Create a new project named `gitlab-workshop`
    * [ ] Initialize the project with a README file
    * [ ] Click on the README file to edit its content
    * [ ] Click the button "Edit" and make some changes
    * [ ] Go back to the project page and copy the URL to download your project using the **clone** button. Make sure to use the HTTPS link.
* Create a local copy to work with your repository
    * [ ] Open the command line interface (GitBash on Windows, Terminal of your choice on MacOS/ Linux)
    * [ ] Create a new, empty folder. E.g. using the command `mkdir workshop`
    * [ ] Change into that folder using the command `cd workshop`
    * [ ] Download your repository using the command `git clone [URL]`
    * [ ] `git` asks for your user name and password. See [Cloning projects via HTTPS](clone_en.md) for details ([German](clone.md)).
    * [ ] Change into your cloned repository folder using the command `cd gitlab-workshop`
* Make some changes to your README file in your local copy of the repository
    * [ ] Commit these changes in your local copy of the repository
    * [ ] Transfer these changes to the GitLab server using `git push origin main`
    * [ ] Check in your browser, if the changes you made locally can be seen on the project page on git.uni-due.de.

    


/confidential
/assign me
/due 2024-04-11
/title Online Preparation

<!--
















You are creating a personal checklist to track your progress for the online preparation.

Click on the button "Create Issue" to create your checklist.

On the following screen, you will see your checklist.











-->
