# Working with GitLab projects

After finished the **REQUIRED** and **SUGGESTED** preparation steps above, please try to work through the following steps.
Try to get as far as possible, but don't worry, all these steps will be repeated in the workshop.

* Log into [GitLab.com](https://gitlab.com)
    * Create a new project named `gitlab-nrw-workshop`
    * Initialize the project with a README file
    * Click on the README file to edit its content
    * Click the button "Edit" and make some changes
    * Go back to the project page and copy the URL to download your project using the **clone** button. Make sure to use the HTTPS link.
* Create a local copy to work with your repository
    * Open the command line interface (GitBash on Windows, Terminal of your choice on MacOS/ Linux)
    * Create a new, empty folder. E.g. using the command `mkdir workshop`
    * Change into that folder using the command `cd workshop`
    * Download your repository using the command `git clone [URL]`
    * `git` asks for your user name and password. See [Cloning projects via HTTPS](clone_en.md) for details ([German](clone.md)).
    * Change into your cloned repository folder using the command `cd gitlab-nrw-workshop`
* Make some changes to your README file in your local copy of the repository
    * Commit these changes in your local copy of the repository
    * Transfer these changes to the GitLab server using `git push origin main`
    * Check in your browser, if the changes you made locally can be seen on the project page on GitLab.com.
