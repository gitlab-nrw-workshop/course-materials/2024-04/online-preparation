# TortoiseGit

Installing tortoiseGit is optional.
To install tortoise, git needs to be already installed.
Visit https://tortoisegit.org to download the installer.


![](./img/1_Download_tort.jpg)

Navigate to the "Download" page ...

![](./img/2_Download_tort.png)

... where you can download the installation files for you operating system.

![](./img/3_Download_tort.png)

Save the file to your computer.

![](./img/4_Install_tort.png)

After the download has finished, start the installer by double clicking the file and pressing the OK button on the dialogue shown above.

![](./img/5_Install_tort.png)

This should start the installation wizard.
Continue by clicking "Next".

![](./img/6_install_tort.png)

Read the licensing agreement and press "Next".

![](./img/7_install_tort.png)

Select the upper option an continue by pressing "Next".

![](./img/8_install_tort.png)

Pick a folder into which tortoiseGit should be installed.

![](./img/9_install_tort.png)

Start the installation by pressing "Install".

![](./img/10_install_tort.png)

Wait for the installation process to finish.

![](./img/11_install_tort.png)

Select the checkbox as shown above and click "Finish".

![](./img/12_install_tort.png)

Select a language for tortoiseGit to use.
We recommend using English.

![](./img/13_install_tort.png)

Continue by pressing "Next".

![](./img/14_install_tort.png)

The path to `git.exe` should be automatically filled by the installer.
If not, enter the path of your git installation here.
Continue by pressing "Next".

![](./img/15_install_tort.png)

Select "Don't store these settings now" and click "Next".

![](./img/17_install_tort.png)

Again, select "Don't store these settings now" and click "Finish".

![](./img/18_Startmenue_tort.png)

TortoiseGit is now installed and can be launched from the start menu.
