* Start
    * [Landing Page](README.md)
    * [My Preparation Checklist :arrow_upper_right:](https://gitlab.com/gitlab-nrw-workshop/course-materials/2024-04/online-preparation/-/issues/new?issuable_template=Preparation&issue[title]=Online+Preparation)
<!-- * Online Preparation -->
    * Installing Git
      * [German 🇩🇪](git-install.md)
      * [English 🇬🇧](git-install_en.md)
<!--     * [Git Basics: Version Control](git-basics.md) -->
<!--     * Collaboration in GitLab -->
<!--         * Creating and configuring GitLab projects [German](projects.md), [English](projects_en.md) -->
<!--         * Documentation, Markdown und Wikis [German](documentation.md) -->
<!--         * Planning and tracking work packages and projects [German](issues.md) -->
<!--         <\!-- * [Codeschnipsel](snippets.md) -\-> -->
<!--         * Cloning projects via HTTPS [German](clone.md), [English](clone_en.md) -->
<!--     * GitLab UIs -->
<!--         * [Installation Guide, VSCodium, English](install_vscode.md) -->
    * First Steps
      * [German 🇩🇪](create-project.md)
      * [English 🇬🇧](create-project_en.md)
