# Git Installation Guide, Windows

Download the installer from the website https://git-scm.com

![](./img/1_Download.png)

Click directly on the download link on the right under the most recent version, or ...

![](./img/2_Download.png)

... navigate to "Downloads" and select the desired version.

![](./img/3_Download.png)

Confirm that you want to save the file and execute the installer, after it has finished downloading.
After the installation routine has started, confirm that the installer can make changes to your system.

![](./img/4_Install_cut.png)

Accept the licensing agreement by pressing "Next".

![](./img/5_Install_cut.png)

On the following page, select an installation path for git.
We strongly recommend sticking with the default path here.
Continue by pressing "Next".

![](./img/6_install_cut.png)

Keep the default component selection and click "Next".

![](./img/7_install_cut.png)

Select under which name git will be available in your start menu.
We recommend using the default here.
Continue by pressing "Next".

![](./img/8_install_cut.png)

Select a text editor that git should use.
While we recommend using the default `vim` here, please be advised that it can require some familiarization.
You can find an overview of commands and a small introduction here: https://www.howtoforge.com/vim-basics

![](./img/9_install_cut.png)

On the next page, keep the default option "Use Git from the Windows command prompt" and click "Next".

![](./img/10_install_cut.png)

On the next page, keep the default option "Use OpenSSH" and click "Next".

![](./img/11_install_cut.png)

On the next page, keep the default option "Use the OpenSSL library" and click "Next".

![](./img/12_install_cut.png)

Select the first option (default) and continue by clicking "Next".

![](./img/13_install_cut.png)

Select the upper option (if it is not selected by default) and continue by clicking "Next".

![](./img/14_install_cut.png)

Keep the default extra options and continue by pressing "Next".

![](./img/15_install_cut.png)

Do not enable any experimental features and continue by clicking "Next".

![](./img/16_install_cut.png)

Wait for the installation to finish.

![](./img/17_install_cut.png)

Close the installer by clicking "Finish".

![](./img/18_StartMenue_cut.png)

You should now find git in the start menu.

![](./img/19_Kontextmenue_cut.png)

Additionally, you will find options to launch git from the context menu (right click).

Congratulations, you successfully installed git on your windows computer.
