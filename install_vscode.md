# Installation Guide VSCodium (or VSCode)

VSCodium is an editor for text files that offers a rich set of additional features and is available for all common operating systems.
Due to its ability to integrate with git, it can be used as a graphical user interface (GUI) to work with git.

Note that if you are already using VSCode you do not need to additionally install VSCodium.
Both tools are mostly identical. For the exact differences please refer to https://vscodium.com/#why.

Installing VSCodium is optional for the workshop.
To install VSCodium, you need to have git already installed on your system.
Find the installation files for your operation System on https://vscodium.com/.
The version numbers at the end of the file names (here 1.65.2) might be slightly different.
Please use the newest version.

* For Windows: VSCodiumUserSetup-x64-1.65.2.exe
* For MacOS: VSCodium-darwin-x64-1.65.2.zip
* For Linux, please adhere to the installation guide for your distribution

Installation on Windows and MacOS usually does not require an admin account.


## Installation on Windows

Execute the installer and accept the licensing agreement.

![](img/2022-03-25-17-04-21.png)

Select a target path for the installation on your system.
We recommend sticking to the default path.

![](img/2022-03-25-17-04-36.png)

Specify if you want a link to VSCodium in your start menu.
We recommend sticking with the default value here.

![](img/2022-03-25-17-04-49.png)

Finally, select if you want to use VSCodium as your default text editor.
If you are using another text editor, like Notepad++, de-select the corresponding checkbox.

![](img/2022-03-25-17-05-00.png)

Take a look at the selected path and installation parameters.
If they are ass expected, click "Install.

![](img/2022-03-25-17-05-17.png)

Start VSCodium e.g. by selecting it from the start menu.

## Installing the `Git Graph` extension

Proceed to install the `Git Graph` extension by selecting "Extensions" from the menu on the left ![](img/2022-03-25-20-25-41.png).

Type `git graph` into the search field and select the `Git Graph` extension by author `mhutchie`.
Then, press the blue install button.

![](img/2022-03-25-17-06-36.png)

This concludes the installation of VSCodium and the required extensions.
